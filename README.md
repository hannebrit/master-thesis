# README #

* Design matrix.R --> R script for the design matrix
* DesignScatter.R --> R script for making figures; 2.4 - 2.7
* HDPLS_pred.R --> This code is written by Kristian Liland
* HDPLS.R --> This code is written by Kristian Liland
* RelCompPlot.R --> This code is written by Solve Sæbø, used for making the figures. 
* relsimclass.R --> The script for design matrix 2
* SolveSim02.R --> This code is written by Solve Sæbø, with modification. This was used to run the Hot PLS and making the Rdata for the analysis. 