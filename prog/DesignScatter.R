library(simrel)
library(pls)
library(MASS)
library(effects)
library(rgl)
library(scatterplot3d)

sim1 <- simrel(n=500, p=10,m=3,q=10,relpos=c(1, 2, 3), gamma=0.9, R2=0.99, ntest=10000)
source("simrelplot2.R")
#simrelplot2(sim1, ask = TRUE)
Y.1 <- sim1$Y
X.1 <- sim1$X
id <- order(Y.1)
Y.1 <- as.matrix(rep(c(1,2),each=500/2))  #The first half as class 1, the other as class 2
X.1 <- X.1[id,,drop=F]          #Sorting the X's accordingly.

Y.1.TEST <- sim1$TESTY
X.1.TEST <- sim1$TESTX
id <- order(Y.1.TEST)
Y.1.TEST <- as.matrix(rep(c(1,2),each=10000/2))
X.1.TEST <- X.1.TEST[id,,drop=F]

#Level 2.1
sim <- simrel(n=250, p=10,m=2,q=10,relpos=c(5, 7), gamma=0.1, R2=0.99, ntest=5000)#,rot=NULL)#sim1$Rotation)
simrelplot2(sim)
Y.21 <- sim$Y
X.21 <- sim$X
id <- order(Y.21)
Y.21 <- as.matrix(rep(c(1,2),each=500/4))  #The first half as class 1, the other as class 2
X.21 <- X.21[id,,drop=F]          #Sorting the X's accordingly.

Y.21.TEST <- sim$TESTY
X.21.TEST <- sim$TESTX
id <- order(Y.21.TEST)
Y.21.TEST <- as.matrix(rep(c(1,2),each=5000/4))
X.21.TEST <- X.21.TEST[id,,drop=F]


#Level 2.2
sim <- simrel(n=250, p=10,m=2,q=10,relpos=c(5, 7), gamma=0.9, R2=0.99, ntest=5000)#,rot=NULL)#sim1$Rotation)
simrelplot2(sim)
Y.22 <- sim$Y
X.22 <- sim$X
id <- order(Y.22)
Y.22 <- as.matrix(rep(c(3,4),each=500/4))  #The first half as class 3, the other as class 4
X.22 <- X.22[id,,drop=F]          #Sorting the X's accordingly.

Y.22.TEST <- sim$TESTY
X.22.TEST <- sim$TESTX
id <- order(Y.22.TEST)
Y.22.TEST <- as.matrix(rep(c(3,4),each=5000/4))
X.22.TEST <- X.22.TEST[id,,drop=F]

Y.2 <- rbind(Y.21, Y.22)
Y.2.TEST <- rbind(Y.21.TEST, Y.22.TEST)
X.2 <- rbind(X.21, X.22)
X.2.TEST <- rbind(X.21.TEST, X.22.TEST)

#Final prepared data
Y <- list(Y.1=Y.1, Y.2=Y.2)
Y.TEST <- list(Y.1=Y.1.TEST, Y.2=Y.2.TEST)
X <- X.1 + X.2
X.TEST <- X.1.TEST + X.2.TEST

#plot(X[,1], Y[[2]], col = c("red", "blue", "dark green", "black"))

# test <- svd(X)
# Z <- X%*%test$v[,1:10]
# # plot(Z[,c(1, 3)],pch=20,cex=2, col=Y$Y.1)
# # #plot(Z[,c(5, 7)],xlab="comp1", ylab="comp2", pch=20,cex=2, col=Y$Y.2)
# # plot(Z[,c(1, 3)],xlab="comp1", ylab="comp2",pch=20,cex=2, col=Y$Y.2)
# lett <- Z[,c(1, 2, 3)]
# # vans <- Z[,c(5, 6, 7)]
# # #
# # #scatterplot3d(points, y = NULL, z = NULL, color=Y$Y.2)
# # 
# # #3D plot med spinning
# plot3d(lett, xlab="comp1", ylab="comp2", zlab="comp3", col=Y$Y.2, size=8)



